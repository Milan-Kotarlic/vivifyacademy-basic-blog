<?php
include('database.php');
$servername = "127.0.0.1";
$username = "root";
$password = "password";
$dbname = "blog";

try {
    $connection = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo $e->getMessage();
}


if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $sqlAuthor = "SELECT id FROM author;";
    $authors = getDataFromDatabase($connection, $sqlAuthor);

$random_author = array_rand(array_map(function ($author) {
    return $author['id'];
}, $authors), 1);

if ($_POST['comment']) {
    $sql = "INSERT INTO comments (author_id, text, post_id) VALUES ({$authors[$random_author]['id']}, '{$_POST['comment']}', {$_GET['post_id']});";
    $statement = $connection->prepare($sql);
    $statement->execute();
    header("Location: single-post.php?post_id={$_GET['post_id']}");
}
}
?>

<!doctype html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>Vivify Blog</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="styles/blog.css" rel="stylesheet">
    <link href="styles/styles.css" rel="stylesheet">
</head>

<body>
    <?php include('header.php') ?>
    <main role="main" class="container">
        <div class="row">
            <div class="col-sm-8 blog-main">
                <?php


                if (isset($_GET['post_id'])) {
                    $sql = "SELECT id, title, body, created_at, author_id, post_image FROM posts WHERE posts.id = {$_GET['post_id']}";
                    $statement = $connection->prepare($sql);
                    $statement->execute();
                    $statement->setFetchMode(PDO::FETCH_ASSOC);
                    $singlePost = $statement->fetch();
                    $postId = ($singlePost['author_id']);

                    $sq = "SELECT name, last_name, gender FROM author WHERE id = $postId";

                    $state = $connection->prepare($sq);

                    $state->execute();

                    $state->setFetchMode(PDO::FETCH_ASSOC);

                    $author = $state->fetchAll();
                ?>
                    <div class="blog-post">
                        <h2 class="blog-post-title"><?php echo ($singlePost['title']) ?></h2>
                        <p class="blog-post-meta"><?php echo ($singlePost['created_at']) ?> by
                        <a href="#"  class="<?php if($author[0]['gender'] === 'M') { echo 'is-male'; }
                        else if(($author[0]['gender'] === 'F')) { echo 'is-female';} ?>"><?php echo($author[0]['name']) . ' ' . ($author[0]['last_name']); ?></a></p>

                        <p><?php echo ($singlePost['body']) ?> </p>
                        <?php if ($singlePost['post_image'] !== null) { ?> <img class="blog-image" src=<?php echo ($singlePost['post_image']) ?> /> <?php } ?>
                    </div>
                    <?php
                    $comments = 'comments.php';
                    include($comments) ?>
            </div>
            <?php include('sidebar.php') ?>
        <?php } ?>
        </div>
    </main>
    <?php include('footer.php') ?>
</body>

</html>