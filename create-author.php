<?php
include('database.php');
$page = 'create-author.php';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$name = $_POST['name'];
	$lastName = $_POST['last_name'];
	$gender = $_POST['gender'];
	$sql = "INSERT INTO author (name, last_name, gender) VALUES ('$name', '$lastName', '$gender')";
	insertIntoDB($connection, $sql);
	header('location: index.php');
}
?>

<!doctype html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="../../../../favicon.ico">

	<title>Vivify Blog</title>

	<!-- Bootstrap core CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

	<!-- Custom styles for this template -->
	<link href="styles/blog.css" rel="stylesheet">
	<link href="styles/styles.css" rel="stylesheet">
</head>
<body>
	<?php include('header.php') ?>
	<main role="main" class="container">
		<h1>Create new author</h1>
		<form action="create-author.php" method="POST" class="form">
			<div class="form-group">
				<label>Name</label>
				<input type="text" class="form-control" name="name" placeholder="Enter name" />
			</div>

			<div class="form-group"><label>Last Name</label>
				<input type="text" class="form-control" name="last_name" placeholder="Enter your last name" />
			</div>
			<div class="form-group">
				<label>Gender</label>
				<div class="form-check">
					<input class="form-check-input" type=radio name="gender" value="M" />
					<label class="form-check-label" for="exampleRadios1">
						Male
					</label>
				</div>
				<div class="form-check">
					<input class="form-check-input" type=radio name="gender" value="F" />
					<label class="form-check-label" for="exampleRadios1">
						Female
					</label>
				</div>
			</div>
			<button type="submit" class="btn btn-primary">Create author</button>
		</form>
		</div>

	</main>

	<?php include('footer.php') ?>

</body>

</html>