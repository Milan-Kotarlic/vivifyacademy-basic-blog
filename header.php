
<header>
    <div class="blog-masthead header">
        <div class="container">
            <nav class="nav">
                <a class="nav-link <?php echo ($page == "index.php" ? "active" : "") ?>" href="index.php">Home</a>
                <a class="nav-link <?php echo ($page == "create-post.php" ? "active" : "") ?>" href="create-post.php">New post</a>
                <a class="nav-link  <?php echo ($page == "create-author.php" ? "active" : "") ?>"  href="create-author.php">New author</a>
                <a class="nav-link" href="#">About</a>
            </nav>
        </div>
    </div>

    <div class="blog-header">
        <div class="container">
            <h1 class="blog-title">The Asignment Blog</h1>
            <p class="lead blog-description">An example blog template built with Bootstrap.</p>
        </div>
    </div>
</header>