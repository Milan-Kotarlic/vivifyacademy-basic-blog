<?php
include('database.php');
$page = 'index.php';
?>

<!doctype html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>Vivify Blog</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="styles/blog.css" rel="stylesheet">
    <link href="styles/styles.css" rel="stylesheet">
</head>

<body>
    <?php include('header.php') ?>
    <main role="main" class="container">
        <div class="row">
            <div class="col-sm-8 blog-main">
                <div class="blog-post">
                    <?php
                    $sql = "SELECT id, title, LEFT(body, 100) AS fmt_body, author_id, post_image, DATE_FORMAT(created_at, '%e %b %Y') AS fmt_created_at FROM posts ORDER BY created_at DESC";
                    $statement = $connection->prepare($sql);
                    $statement->execute();
                    $statement->setFetchMode(PDO::FETCH_ASSOC);
                    $posts = $statement->fetchAll();
                    ?>
                    <?php
                    foreach ($posts as $post) {
                        $postId = ($post['author_id']);
                        $sq = "SELECT name, last_name, gender FROM author WHERE id = $postId";
                        $state = $connection->prepare($sq);
                        $state->execute();
                        $state->setFetchMode(PDO::FETCH_ASSOC);
                        $author = $state->fetchAll();
                    ?>
                        <a class="blog-post-title" href="single-post.php?post_id=<?php echo ($post['id']) ?>"><?php echo ($post['title']) ?></a>
                        <p class="blog-post-meta"><?php echo ($post['fmt_created_at']); ?> by
                            <a href="#" class="<?php if ($author[0]['gender'] === 'M') {
                                                    echo 'is-male';
                                                } else if (($author[0]['gender'] === 'F')) {
                                                    echo 'is-female';
                                                } ?>">
                                <?php echo ($author[0]['name']) . ' ' . ($author[0]['last_name']); ?></a>
                        </p>
                        <p class="blog-post-text"><?php echo ($post['fmt_body']) ?>...</p>
                        <img class="blog-image" src="<?php echo ($post['post_image']) ?>" />
                        <hr />
                    <?php } ?>
                </div>
            </div>
            <?php include('sidebar.php') ?>
    </main>
    <?php include('footer.php') ?>
</body>

</html>