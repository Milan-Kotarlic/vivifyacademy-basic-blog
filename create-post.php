<?php
include('database.php');
$page = 'create-post.php';

$sql = "SELECT id, name, last_name FROM author";

$statement = $connection->prepare($sql);

$statement->execute();

$statement->setFetchMode(PDO::FETCH_ASSOC);

$authors = $statement->fetchAll();


if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$title = $_POST['title'];
	$text = $_POST['text'];
	$author = $_POST['author'];
	$image = $_POST['image'];
	$created_at_raw = htmlentities($_POST['created_at']);
	$created_at = date('Y-m-d', (strtotime($expires_on_raw)));
	$sql = "INSERT INTO posts (title, body, created_at, post_image, author_id) VALUES ('$title', '$text', now(), '$image', '$author')";
	insertIntoDB($connection, $sql);
	header('location: index.php');
}

?>

<!doctype html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="../../../../favicon.ico">

	<title>Vivify Blog</title>

	<!-- Bootstrap core CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

	<!-- Custom styles for this template -->
	<link href="styles/blog.css" rel="stylesheet">
	<link href="styles/styles.css" rel="stylesheet">
</head>

<body>

	<?php include('header.php') ?>

	<main role="main" class="container">
	<h1>Create new post</h1>
		<form action="create-post.php" method="POST" class="form">
			<div class="form-group">
				<label>Title</label>
				<input type="text" class="form-control" name="title" placeholder="Enter blog title" />
			</div>

			<div class="form-group"><label>Author</label>
			<select class="form-control" name="author"
				 id="author_id" placeholder="Enter your name" >
				<option>Select author</option>
			<?php foreach ($authors as $author) {
						?> <option value="<?php echo ($author['id']) ?>">
							<?php
							 echo ($author['name']) . ' ' . ($author['last_name']);
							?></option>
					<?php } ?>
				</select>
			</div>

			<div class="form-group"><label>Image</label>
				<input type="text" class="form-control" name="image" placeholder="Enter image url" />
			</div>

			<div class="form-group"><label>Text</label>
				<textarea class="form-control" name="text" rows="20" placeholder="Your post..."></textarea>
			</div>

			<button type="submit" class="btn btn-primary">Submit post</button>
		</form>
		</div>

	</main>

	<?php include('footer.php') ?>

</body>

</html>